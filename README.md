## Multi-Flat Shipping

This extension will create 5 flat rate shipping methods with different prices and conditions.
It will also add a custom HTML message you can show in the checkout page to inform your customers about your shipping methods.
IDEALIAGroup IG MultiFlat Shipping is UPDATE SAFE since no system files are replaced or modified and you can enable or disable it in a click.
IMPORTANT: Disable your cache before installing this extension!

MAIN FEATURES:
* FULL MAGENTO BACKEND INTEGRATION: all parameters available in configuration (no coding needed)
* Available for all languages via Magento Multi Store full support
* Custom checkout message on shipping method selection with full support to HTML
* Up to 5 Flat rate shipping methods with different pricing and conditions

IG Multi-Flat Shipping is a MageSpecialist extension.